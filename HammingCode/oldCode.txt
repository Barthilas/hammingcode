﻿//var H = Matrix<double>.Build;
            //Ver1
            //double[,] _H = { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
            //                 { 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0 },
            //                 { 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0 },
            //                 { 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0 },
            //                 { 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1 }};

		var CheckMatrix = H.DenseOfArray(_H);

//var G = Matrix<double>.Build;
            //ver1
            //double[,] _G = {{ 1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0 },
            //                { 0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1 },
            //                { 0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1 },
            //                { 0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0 },
            //                { 0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,1 },
            //                { 0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,0 },
            //                { 0,0,0,0,0,0,1,0,0,0,0,0,0,1,1,1 },
            //                { 0,0,0,0,0,0,0,1,0,0,0,0,1,1,0,1 },
            //                { 0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1 },
            //                { 0,0,0,0,0,0,0,0,0,1,0,0,1,0,1,0 },
            //                { 0,0,0,0,0,0,0,0,0,0,1,0,1,0,1,1 }};

			var BuildMatrix = H.DenseOfArray(_G);


		var inputBuilder = Matrix<double>.Build;
            double[,] x = { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } };
            var InputMatrix = H.DenseOfArray(x);


            var vysledek = InputMatrix * BuildMatrix;

            ModNOfMatrix(ref vysledek);

            var failedBuilder = Matrix<double>.Build;
            double[,] y = { { 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0 } }; //ver 1 { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,0,0,1,1,1 } 
            var failedMatrix = inputBuilder.DenseOfArray(y);

            var syndrom = CheckMatrix * vysledek.Transpose();
            var failed = CheckMatrix * failedMatrix.Transpose();
            var checkInitMatrixes = BuildMatrix * CheckMatrix.Transpose(); //this should be 0 at all times. Else the G or H is incorrect

            //failed.Column(1) == CheckMatrix.Column(1);

			            //Controller.HammingController.testBasicMatrixes();

            //double[,] x = { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } };
            //var vysledek = Controller.HammingController.BuildHammingCode(x);

            //double[,] y = { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0 } };
            //var error = Controller.HammingController.DoubleMatrixBuilder.DenseOfArray(y);

            //var syndrom = Controller.HammingController.BuildSyndrome(vysledek);
            //var failed = Controller.HammingController.BuildSyndrome(error);

            //var a = Controller.HammingController.CheckSyndrome(syndrom, vysledek);
            //var b = Controller.HammingController.CheckSyndrome(failed, error);
            ////failed.Column(1) == CheckMatrix.Column(1);
