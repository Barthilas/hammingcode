﻿//using MathNet.Numerics.LinearAlgebra;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace HammingCode.Controller
//{
//    public static class HammingController
//    {
//        public static MatrixBuilder<double> DoubleMatrixBuilder = Matrix<double>.Build;
//        public static VectorBuilder<double> DoubleVectorBuilder = Vector<double>.Build;

//        //private static double[,] H = { { 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0 },
//        //                               { 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0 },
//        //                               { 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0 },
//        //                               { 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0 },
//        //                               { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }};
//        private static double[,] H = {  { 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1},
//                                        { 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0},
//                                        { 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0},
//                                        { 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0} };
//        public static Matrix<double> CheckMatrix = DoubleMatrixBuilder.DenseOfArray(H);

//        //private static double[,] G = {{ 1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0 },
//        //                            { 0,1,0,0,0,0,0,0,0,0,0,1,0,1,1,0 },
//        //                            { 0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0 },
//        //                            { 0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0 },
//        //                            { 0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0 },
//        //                            { 0,0,0,0,0,1,0,0,0,0,0,0,1,1,0,0 },
//        //                            { 0,0,0,0,0,0,1,0,0,0,0,1,1,1,0,0 },
//        //                            { 0,0,0,0,0,0,0,1,0,0,0,1,1,1,1,0 },
//        //                            { 0,0,0,0,0,0,0,0,1,0,0,1,0,0,1,0 },
//        //                            { 0,0,0,0,0,0,0,0,0,1,0,0,1,0,1,0 },
//        //                            { 0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,0 }};
//        private static double[,] G = {{ 1,0,0,0,0,0,0,0,0,0,0,0,1,1,1 },
//                                      { 0,1,0,0,0,0,0,0,0,0,0,1,0,1,1 },
//                                      { 0,0,1,0,0,0,0,0,0,0,0,1,1,0,1 },
//                                      { 0,0,0,1,0,0,0,0,0,0,0,1,1,1,0 },
//                                      { 0,0,0,0,1,0,0,0,0,0,0,1,1,1,1 },
//                                      { 0,0,0,0,0,1,0,0,0,0,0,1,1,0,0 },
//                                      { 0,0,0,0,0,0,1,0,0,0,0,1,0,1,0 },
//                                      { 0,0,0,0,0,0,0,1,0,0,0,1,0,0,1 },
//                                      { 0,0,0,0,0,0,0,0,1,0,0,0,1,1,0 },
//                                      { 0,0,0,0,0,0,0,0,0,1,0,0,1,0,1 },
//                                      { 0,0,0,0,0,0,0,0,0,0,1,0,0,1,1 }};
//        public static Matrix<double> BuildMatrix = DoubleMatrixBuilder.DenseOfArray(G);

//        public static Matrix<double> BuildHammingCode(double[,] input)
//        {
//            var InputMatrix = DoubleMatrixBuilder.DenseOfArray(input);
//            var EncodedMatrix = InputMatrix * BuildMatrix;
//            ModNOfMatrix(ref EncodedMatrix);
//            AddExtraParityBit(ref EncodedMatrix);
//            return EncodedMatrix;
//        }
//        /// <summary>
//        /// Adds 0 if number of '1s' is even.
//        /// </summary>
//        /// <param name="encoded"></param>
//        /// <returns></returns>
//        private static void AddExtraParityBit(ref Matrix<double> encoded)
//        {
//            var expandedMatrix = DoubleMatrixBuilder.Dense(1, 16, encoded.Storage);
//            double sum = 0;
//            double parity;
//            for (int i=0;i<encoded.ColumnCount;i++)
//            {
//                sum += encoded[0, i];
//            }
//            if (sum%2==0)
//            {
//                parity = 0;
//            }
//            else
//            {
//                parity = 1;
//            }

//            var vector = DoubleVectorBuilder.Dense(new double[] { parity });
//            encoded.
//            encoded.InsertColumn(encoded.ColumnCount+1, vector);
//        }
//        public static Matrix<double> BuildSyndrome(Matrix<double> input)
//        {
//            var syndrom = CheckMatrix * input.Transpose();
//            return syndrom;
//        }
//        /// <summary>
//        /// Changes the same array(hence ref) to mod of base N
//        /// </summary>
//        /// <param name="m">Matrix input</param>
//        /// <param name="mod">Modulo by</param>
//        public static void ModNOfMatrix(ref Matrix<double> m, int mod = 2)
//        {
//            //Matrix<double> modMatrix = H.DenseOfMatrix(m);
//            for (int i = 0; i < m.RowCount; i++)
//            {
//                for (int j = 0; j < m.ColumnCount; j++)
//                {
//                    m[i, j] = m[i, j] % mod;
//                }
//            }
//        }
//        /// <summary>
//        /// 4 cases
//        /// a) syndrome = 0 ; parity = 0 -> no error.
//        /// b) syndrome = 0 ; parity = 1 -> error in parity.
//        /// c) syndrome = 1 ; parity = 1 -> single bit error can be corrected. Syndrome has position.
//        /// d) syndrome = 1 ; parity = 0 -> Double error.
//        /// </summary>
//        /// <param name="syndrome"></param>
//        /// <returns></returns>
//        public static bool CheckSyndrome(Matrix<double> syndromeMatrix, Matrix<double> dataMatrix)
//        {
//            bool isCorrect = true;
//            double tmp = 0;
//            double parity = CalculateParity(dataMatrix);
//            ModNOfMatrix(ref syndromeMatrix);
//            for (int i = 0; i < syndromeMatrix.RowCount; i++)
//            {
//                tmp += syndromeMatrix[i, 0];
//            }
//            int syndrome = (int)tmp % 2;
//            if (syndrome != 0)
//            {
//                isCorrect = false;
//            }
//            return isCorrect;
//        }
//        public static double CalculateParity(Matrix<double> dataMatrix)
//        {
//            double result = 0;
//            for(int i=11;i<dataMatrix.ColumnCount;i++)
//            {
//                result += dataMatrix[0, i];
//            }
//            return result%2;
//        }
//        public static void testBasicMatrixes()
//        {
//            //BuildMatrix * CheckMatrix.Transpose(); //this should be 0 at all times. Else the G or H is incorrect
//            var test = BuildMatrix * CheckMatrix.Transpose();
//        }
//    }
//}
