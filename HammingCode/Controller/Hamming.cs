﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HammingCode.Controller
{
    /// <summary>
    /// The 16,11 extended version 15,11 of Hamming. 11 data carriers, 4 parity bits + 1 overall parity bit for double error detection.
    /// </summary>
    public static class Hamming
    {   /// <summary>
        /// on power of 2 (1,2,4,8...) adds redundant(parity) even bits.
        /// </summary>
        /// <param name="input"></param>
        public static void AppendRedundantBits(ref List<bool> input)
        {
            if(input.Count!=HammingConstants.inputLength)
            {
                throw new ArgumentException("Not a valid Hamming 16,11 input.");
            }

            int r1 = 0;
            for (int i = 0; i < HammingConstants.redundant1Cover.Length; i++)
            {
                int position = HammingConstants.redundant1Cover[i];
                int bit = input[position] ? 1 : 0;
                r1 += bit;
            }
            bool result1 = r1 % 2 == 0 ? false : true;

            int r2 = 0;
            for (int i = 0; i < HammingConstants.redundant2Cover.Length; i++)
            {
                int position = HammingConstants.redundant2Cover[i];
                int bit = input[position] ? 1 : 0;
                r2 += bit;
            }
            bool result2 = r2 % 2 == 0 ? false : true;

            int r3 = 0;
            for (int i = 0; i < HammingConstants.redundant3Cover.Length; i++)
            {
                int position = HammingConstants.redundant3Cover[i];
                int bit = input[position] ? 1 : 0;
                r3 += bit;
            }
            bool result3 = r3 % 2 == 0 ? false : true;

            int r4 = 0;
            for (int i = 0; i < HammingConstants.redundant4Cover.Length; i++)
            {
                int position = HammingConstants.redundant4Cover[i];
                int bit = input[position] ? 1 : 0;
                r4 += bit;
            }
            bool result4 = r4 % 2 == 0 ? false : true;

            input.Insert(HammingConstants.redundant1Position, result1);
            input.Insert(HammingConstants.redundant2Position, result2);
            input.Insert(HammingConstants.redundant3Position, result3);
            input.Insert(HammingConstants.redundant4Position, result4);

            AddExtraParity(ref input);
        }
        private static void AddExtraParity(ref List<bool> input)
        {
            var r = 0;
            for (int i = 0; i < input.Count; i++)
            {
                int bit = input[i] ? 1 : 0;
                r += bit;
            }
            bool result = r % 2 == 0 ? false : true;
            input.Insert(HammingConstants.extraParityPosition, result);

        }
        /// <summary>
        /// Checks syndrome and do self-repair if neccesary.
        /// </summary>
        /// <param name="inputEncoded"></param>
        public static List<bool> CheckSyndrome(List<bool> inputEncoded)
        {
            if (inputEncoded.Count != HammingConstants.outputLength)
            {
                throw new ArgumentException("Input is expected to be of 16 length.");
            }

            List<bool> result = new List<bool>();
            var c1 = 0;
            for (int i = 0; i < HammingConstants.redundant1Check.Length; i++)
            {
                int position = HammingConstants.redundant1Check[i];
                int bit = inputEncoded[position] ? 1 : 0;
                c1 += bit;
            }
            c1 = c1 % 2;

            var c2 = 0;
            for (int i = 0; i < HammingConstants.redundant2Check.Length; i++)
            {
                int position = HammingConstants.redundant2Check[i];
                int bit = inputEncoded[position] ? 1 : 0;
                c2 += bit;
            }
            c2 = c2 % 2;

            var c3 = 0;
            for (int i = 0; i < HammingConstants.redundant3Check.Length; i++)
            {
                int position = HammingConstants.redundant3Check[i];
                int bit = inputEncoded[position] ? 1 : 0;
                c3 += bit;
            }
            c3 = c3 % 2;

            var c4 = 0;
            for (int i = 0; i < HammingConstants.redundant4Check.Length; i++)
            {
                int position = HammingConstants.redundant4Check[i];
                int bit = inputEncoded[position] ? 1 : 0;
                c4 += bit;
            }
            c4 = c4 % 2;

            var extendedParity = CheckExtraParity(inputEncoded);
            var syndrome = c1 + c2 + c3 + c4;

            //No error
            if (syndrome==0 && extendedParity)
            {
                result = RemoveRedundantBits(inputEncoded);
            }
            //Error in parity. Data is untouched.
            if (syndrome==0 && !extendedParity)
            {
                result = RemoveRedundantBits(inputEncoded);
            }
            //Single bit error can be corrected. Syndrome has position.
            if (syndrome!=0 && !extendedParity)
            {
                int b1 = c1 == 0 ? 0 : 1;
                int b2 = c2 == 0 ? 0 : 2;
                int b3 = c3 == 0 ? 0 : 4;
                int b4 = c4 == 0 ? 0 : 8;
                int _ = b1 + b2 + b3 + b4;

                inputEncoded[_ - 1] = !inputEncoded[_ - 1];
                result = RemoveRedundantBits(inputEncoded);
            }
            //Double error detection
            if (syndrome != 0 && extendedParity)
            {
                Console.WriteLine("Double Error detected.");
                //throw new NotImplementedException("Why hello there.");
            }

            return result;
        }/// <summary>
         /// Based on encoding the extra parity bit should be 0 if number of '1s! is odd. Returns True if parity is the same.
         /// </summary>
         /// <param name="inputEncoded"></param>
        private static bool CheckExtraParity(List<bool> inputEncoded)
        {
            var r = 0;
            for (int i = 0; i < inputEncoded.Count-2; i++)
            {
                int bit = inputEncoded[i] ? 1 : 0;
                r += bit;
            }
            bool parityBit = r % 2 == 0 ? false : true;
            return parityBit == inputEncoded[inputEncoded.Count-1];
        }
        private static List<bool> RemoveRedundantBits(List<bool> encoded)
        {
            List<bool> originalData = new List<bool>();
            for(int i =0;i<encoded.Count;i++)
            {
                if(i != HammingConstants.redundant1Position && i != HammingConstants.redundant2Position && i != HammingConstants.redundant3Position && i != HammingConstants.redundant4Position && i != HammingConstants.extraParityPosition)
                {
                    originalData.Add(encoded[i]);
                }
            }
            return originalData;
        
        }
        public static void SimulateError(ref List<bool> inputEncoded, int position)
        {
            inputEncoded[position] = !inputEncoded[position];
        }
    }
    /// <summary>
    /// https://en.wikipedia.org/wiki/Hamming_code
    /// </summary>
    public static class HammingConstants
    {
        public const int inputLength = 11;
        public const int outputLength = 16;

        public const int redundant1Position = 0;
        public const int redundant2Position = 1;
        public const int redundant3Position = 3;
        public const int redundant4Position = 7;
        public const int extraParityPosition = 15;

        
        public static int[] redundant1Cover = new int[] { 0, 1, 3, 4, 6, 8, 10 }; //Covers (p1), d1, d2, d4, d5, d7, d9, d11
        public static int[] redundant2Cover = new int[] { 0, 2, 3, 5, 6, 9, 10 }; //Covers (p2), d1, d3, d4, d6, d7, d10, d11
        public static int[] redundant3Cover = new int[] { 1, 2, 3, 7, 8, 9, 10}; //Covers (p4), d2, d3, d4, d8, d9, d10, d11 
        public static int[] redundant4Cover = new int[] { 4, 5, 6, 7, 8, 9, 10 }; //Covers (p8), d5-11

        public static int[] redundant1Check = new int[] {0, 2, 4, 6, 8, 10, 12, 14 };
        public static int[] redundant2Check = new int[] {1, 2, 5, 6, 9, 10, 13, 14 };
        public static int[] redundant3Check = new int[] {3, 4, 5, 6, 11, 12, 13, 14 };
        public static int[] redundant4Check = new int[] {7, 8, 9, 10, 11, 12, 13, 14 };
    }
}
