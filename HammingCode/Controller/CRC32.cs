﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HammingCode.Controller
{
    /// <summary>
    /// https://stackoverflow.com/questions/2587766/how-is-a-crc32-checksum-calculated
    /// https://rosettacode.org/wiki/CRC-32
    /// https://www.autohotkey.com/boards/viewtopic.php?f=7&t=35671
    /// 
    ///       QUOTIENT
    //       ----------
    //DIVISOR ) DIVIDEND
    //                = REMAINDER
    //    
    /// </summary>
    public static class CRC32
    {
        public const int length = 32;
        /// <summary>
        /// IEEE802.3, CRC-32
        /// </summary>
        public static List<bool> polynomial = new List<bool>(new bool[] { true, false, false, false, false, false, true, false, false, true, true, false, false, false, false, false, true, false, false, false, true, true, true, false, true, true, false, true, true, false, true, true, true });
        public static List<bool> init = new List<bool>(new bool[] { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true });
        public static List<bool> FormatInput(List<bool> input)
        {
            //reverse bits in each byte
            var bytes = Helpers.StackOverflow.SplitBooleansToBytes(input);
            for(int i=0;i<bytes.Length;i++)
            {
                bytes[i] = Helpers.StackOverflow.ReverseWithUnrolledLoop(bytes[i]);
            }
            //append 32 0 bits
            var bytesWithAppend = Helpers.StackOverflow.ByteArray2ListBoolean(bytes);
            for (int i = 0; i < length; i++)
            {
                bytesWithAppend.Add(false);
            }
            //XOR the first 4 bytes with 0xFFFFFFFF
            //BitArray a = new BitArray(neco.ToArray());
            var result = XOR(bytesWithAppend, init);
            return result;
        }
        /// <summary>
        /// Also "add without carry" or mod2
        /// </summary>
        /// <param name="dividend"></param>
        /// <param name="divisor"></param>
        private static List<bool> XOR(List<bool> dividend, List<bool> divisor)
        {
            var length = dividend.Count;
            List<bool> result = new List<bool>(divisor);
            if(dividend.Count>divisor.Count)
            {
                length = divisor.Count;
                result = new List<bool>(dividend);
            }
            for(int i=0;i<length;i++)
            {
                int v1 = dividend[i] ? 1 : 0;
                int v2 = divisor[i] ? 1 : 0;
                int xor = (v1 + v2) % 2;
                bool _xor = xor==1 ? true : false;
                result[i] = _xor;
            }
            return result;
        }
        public static List<bool> Get(List<bool> input, out byte[] crc32)
        {
            List<bool[]> remainders = new List<bool[]>();
            while(input.Count>=polynomial.Count)
            {
                if(input[0] != true)
                {
                    input.RemoveAt(0); //shift
                }
                else
                {
                    var result = XOR(input, polynomial);
                    remainders.Add(result.ToArray());
                    input = result;
                }
            }
            List<bool> finalRemainder = new List<bool>(remainders[remainders.Count - 1]);
            while (finalRemainder.Count> length)
            {
                //remove leading zeros
                finalRemainder.RemoveAt(0);
            }
            //XOR the first 4 bytes with 0xFFFFFFFF
            var xor1 = XOR(finalRemainder, init);
            //reverse bits in each byte
            var bytes = Helpers.StackOverflow.SplitBooleansToBytes(xor1);
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Helpers.StackOverflow.ReverseWithUnrolledLoop(bytes[i]);
            }
            byte[] reversed = bytes.Reverse().ToArray(); //this needs to be reversed.
            var bytesList = Helpers.StackOverflow.ByteArray2ListBoolean(reversed);
            crc32 = reversed;

            //Console.WriteLine(Helpers.StackOverflow.ByteArrayToString(reversed));
            return bytesList;

        }
    }
}
