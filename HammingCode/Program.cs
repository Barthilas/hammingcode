﻿using CommandLine;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HammingCode
{/// <summary>
/// Hamming extended (16,11)
/// 11 - Data bits
/// 16 - All bits
/// 5 - Parity bits positions 1,2,4,8,16...
/// </summary>
    class Program
    {
        public static int BLOCK = 8192;
        public static int DATABITS = 11;
        public static int CRCHamming = 48; //33/11*16
        static void Main(string[] args)
        {

            Parser.Default.ParseArguments<Options>(args)
              .WithParsed(opts => RunOptionsAndReturnExitCode(opts))
              .WithNotParsed((errs) => HandleParseError(errs));

        }
        private static void HandleParseError(IEnumerable<Error> errs)
        {
            Console.WriteLine("Bad parameters. Try again.");
        }
        private static void RunOptionsAndReturnExitCode(Options opts)
        {
            if (opts.Mode == 'k')
            {
                //remove existing output File
                if (File.Exists(opts.OutputFile))
                {
                    File.Delete(opts.OutputFile);
                }

                var output = new List<bool>();
                var fileToProcess = File.ReadAllBytes(opts.InputFile);
                var fileByChunks = Helpers.StackOverflow.ChunkTrivialBetter(fileToProcess, BLOCK);
                var current11Bits = new List<bool>();

                //Split to List
                List<bool[]> bitsArray = new List<bool[]>();
                foreach (var item in fileByChunks)
                {
                    var array = Helpers.StackOverflow.ByteArray2ListBoolean(item.AsEnumerable().ToArray());
                    bitsArray.Add(array.ToArray());
                }
                //Foreach splitted block
                foreach(var block in bitsArray)
                {
                    output.Clear();
                    for (int i=0;i<block.Length;i++)
                    {
                        if(i%DATABITS==0 && i!=0)
                        {
                            Controller.Hamming.AppendRedundantBits(ref current11Bits);
                            output.AddRange(current11Bits);
                            current11Bits.Clear();

                        }
                        current11Bits.Add(block[i]);
                    }
                    //remainder do smth.
                    if (current11Bits.Count != 0)
                    {
                        while (current11Bits.Count < DATABITS)
                        {
                            current11Bits.Add(false);
                        }
                        Controller.Hamming.AppendRedundantBits(ref current11Bits);
                        output.AddRange(current11Bits);
                        current11Bits.Clear();
                    }

                    //Hamming is done -> append next CRC32
                    var CRC32prep = Controller.CRC32.FormatInput(output);
                    byte[] CRC32Byte;
                    var CRC32 = Controller.CRC32.Get(CRC32prep, out CRC32Byte);
                    //Apply hamming to CRC32
                    CRC32.Add(false); //32%11 != 0 append false to end -> 33%11==0

                    var CRC32Hamming = new List<bool>();
                    for (int i = 0; i < CRC32.Count; i++)
                    {
                        if (i% DATABITS == 0 && i!= 0)
                        {
                            Controller.Hamming.AppendRedundantBits(ref current11Bits);
                            CRC32Hamming.AddRange(current11Bits);
                            current11Bits.Clear();
                        }
                        current11Bits.Add(CRC32[i]);
                    }
                    //flush rest of iteration
                    Controller.Hamming.AppendRedundantBits(ref current11Bits);
                    CRC32Hamming.AddRange(current11Bits);
                    current11Bits.Clear();
                    //Append Hamminged CRC32 to output
                    output.AddRange(CRC32Hamming);

                    if (opts.OutputFile == null)
                    {
                        Console.WriteLine("CRC32");
                        Console.WriteLine(Helpers.StackOverflow.ByteArrayToString(CRC32Byte));
                    }
                    else
                    {
                        Console.WriteLine("File created.");
                        Helpers.StackOverflow.WriteToFile(output, opts.OutputFile);
                    }
                }
            }
            if (opts.Mode == 'd')
            {
                var input = Helpers.StackOverflow.ReadFile(opts.InputFile);

                foreach (var block in input)
                {
                    var CRC32 = new List<bool>();
                    var OriginalData = new List<bool>();
                    var CRC32Hamming = new List<bool>();
                    var current16bits = new List<bool>();
                    var stop = block.Count;
                    //Separate CRC32 (length 48 always)
                    for (int i = 0; i < CRCHamming; i++)
                    {
                        int index = (block.Count - CRCHamming + i);
                        CRC32Hamming.Add(block[index]);
                    }
                    //CRC can be safely deleted.
                    for (int i = 0; i < CRCHamming; i++)
                    {
                        block.RemoveAt(block.Count-1);
                    }
                    //check CRC
                    for (int i = 0; i < CRC32Hamming.Count; i++)
                    {
                        if (i % Controller.HammingConstants.outputLength == 0 && i != 0)
                        {
                            var result = Controller.Hamming.CheckSyndrome(current16bits);
                            CRC32.AddRange(result);
                            current16bits.Clear();
                        }
                        current16bits.Add(CRC32Hamming[i]);
                    }
                    //As always there is remainder
                    var remainder = Controller.Hamming.CheckSyndrome(current16bits);
                    CRC32.AddRange(remainder);
                    CRC32.RemoveAt(CRC32.Count - 1); //Remove the artificially added 0 for hamming.
                    current16bits.Clear();
                    var CRC32ByteOriginal = Helpers.StackOverflow.SplitBooleansToBytes(CRC32);

                    //CALCULATE CRC32 for Original Hamming data
                    var CRC32prep = Controller.CRC32.FormatInput(block);
                    byte[] CRC32ByteCalculated;
                    var CRC32d = Controller.CRC32.Get(CRC32prep, out CRC32ByteCalculated);

                    //Compare Calculated and Received CRC32
                    //If there is error, whole block is corrupted.
                    bool isEqual = Enumerable.SequenceEqual(CRC32ByteCalculated, CRC32ByteOriginal);
                    if (!isEqual)
                    {
                        //zapíše celý blok jako 0byty
                        for(int i=0;i<block.Count;i++)
                        {
                            OriginalData.Add(false);
                        }
                    }
                    else
                    {
                        //no error - > decode the rest
                        //Check rest of the data
                        for (int i = 0; i < block.Count; i++)
                        {
                            if (i % Controller.HammingConstants.outputLength == 0 && i != 0)
                            {
                                var result2 = Controller.Hamming.CheckSyndrome(current16bits);
                                OriginalData.AddRange(result2);
                                current16bits.Clear();
                            }
                            current16bits.Add(block[i]);
                        }
                        //Classic remainder
                        var rem = Controller.Hamming.CheckSyndrome(current16bits);
                        OriginalData.AddRange(rem);
                        current16bits.Clear();
                    }
                    var originalDataBytes = Helpers.StackOverflow.SplitBooleansToBytes(OriginalData);
                    string stdOut = Encoding.ASCII.GetString(originalDataBytes);
                    //For some reason there are unknown characters at the begginning.
                    var resultTrimmed = stdOut.Remove(0, 3);
                    if (opts.OutputFile == null)
                    {
                        //REMOVE APPENDED REDUNDANT BITS AT THE END
                        //while (OriginalData.Count % 8 != 0)
                        //{
                        //    if (OriginalData[OriginalData.Count - 1] == false)
                        //    {
                        //        OriginalData.RemoveAt(OriginalData.Count - 1);
                        //    }
                        //    else
                        //    {
                        //        break;
                        //    }
                        //}
                        Console.WriteLine(resultTrimmed);
                    }
                    else
                    {
                        Console.WriteLine("File created.");
                        File.AppendAllText(opts.OutputFile,resultTrimmed);
                    }
                }
            }
        }
    }
    class Options
    {
        [Option('m', "mode", Required = true, HelpText = "k - code | d - decode")]
        public char Mode { get; set; }

        [Option('i', "input", Required = true, HelpText = "Input file to be processed.")]
        public string InputFile { get; set; }

        [Option('o', "output", Required = false, HelpText = "If specified, output is saved to file.")]
        public string OutputFile { get; set; }


    }
}

//string inputPath = @"C:\Users\sneti\Documents\GitLab\hammingcode\HammingCode\TestData\simple.txt";
//var fileToProcess = File.ReadAllBytes(inputPath);
//var bits = new BitArray(fileToProcess);
//List<bool> n = new List<bool>();
//            for(int i=0;i<Controller.HammingConstants.dataLength;i++)
//            {
//                n.Add(bits[i]);
//            }
//            var b = n.ToArray();

//Controller.Hamming.AppendRedundantBits(ref n);
//            //Controller.Hamming.SimulateError(ref n, 0); Controller.Hamming.SimulateError(ref n, 1);
//            var result = Controller.Hamming.CheckSyndrome(n);
//var padded = Controller.CRC32.FormatInput(n);
////Controller.CRC32.CalculateCRC32(padded);

////var luk = Controller.CRC32Simple.PaddData(n);
////Controller.CRC32Simple.CalculateCRC32(luk);
///

//string crctest = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. Aliquam ornare wisi eu metus. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Cras elementum. Etiam bibendum elit eget erat. Aliquam erat volutpat. In dapibus augue non sapien. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Suspendisse nisl. Vivamus luctus egestas leo. Ut tempus purus at lorem. Integer malesuada."; //a works
//byte[] bytes = Encoding.ASCII.GetBytes(crctest);
//var bitsa = Helpers.StackOverflow.ByteArray2ListBoolean(bytes);
//var lala = Controller.CRC32.FormatInput(bitsa);
//Controller.CRC32.Get(lala);
